export const CATEGORIES: string[] = [
    'Food',
    'Transport',
    'Accommodation',
    'Entertainment',
    'Sports',
    'Healthcare',
    'Clothes',
    'Bills',
    'Other'
];
