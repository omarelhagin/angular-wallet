export class Expense {
  name: string;
  amount: number;
  category: string;
  expenseDate: Date;

  constructor(name: string, amount: number, category: string, expenseDate: Date) {
    this.name = name;
    this.amount = amount;
    this.category = category;
    this.expenseDate = expenseDate;
  }
}
