import { Expense } from './expense';

export class User {
  _id: string;
  _rev: string;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  expenses: Expense[] = [];

  constructor(email: string,
    password: string,
    firstName: string,
    lastName: string) {
      this.email = email;
      this.password = password;
      this.firstName = firstName;
      this.lastName = lastName;
    }
}
