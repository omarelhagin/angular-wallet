import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule,
  MatIconModule, MatListModule, MatFormFieldModule,
  MatInputModule, MatSelectModule, MatProgressSpinnerModule,
  MatDatepickerModule, MatNativeDateModule, MatGridListModule, MatCardModule } from '@angular/material';

import { AppComponent } from './components/app.component';
import { SignInComponent } from './components/auth/signin/signin.component';
import { SignUpComponent } from './components/auth/signup/signup.component';
import { ExpensesComponent } from './components/expenses/expenses.component';
import { CloudantService } from './services/cloudant.service';
import { AuthGuard } from './services/auth-guard.service';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { AppConfigService } from './services/app-config.service';
import { HomeComponent } from './components/home/home.component';
import { AnalyticsComponent } from './components/analytics/analytics.component';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: 'expenses', component: ExpensesComponent, canActivate: [AuthGuard] },
      { path: 'analytics', component: AnalyticsComponent, canActivate: [AuthGuard] }
    ]
  },
  { path: 'signup', component: SignUpComponent },
  { path: 'signin', component: SignInComponent },
  { path: 'not-found', component: ErrorPageComponent, data: {errorMessage: 'Page not found!'} },
  { path: '**', redirectTo: '/not-found'}
];

@NgModule({
  declarations: [
    AppComponent,
    ExpensesComponent,
    SignUpComponent,
    SignInComponent,
    ErrorPageComponent,
    HomeComponent,
    AnalyticsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatCardModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule
  ],
  providers: [CloudantService, AuthGuard, AppConfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
