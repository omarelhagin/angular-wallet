import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  private _loggedInUser: User;

  constructor() {}

  get loggedInUser(): User {
    const userFromStorage = sessionStorage.getItem('user');
    if (!this._loggedInUser && userFromStorage) {
      this._loggedInUser = JSON.parse(userFromStorage);
    }

    return this._loggedInUser;
  }

  set loggedInUser(user: User) {
    this._loggedInUser = user;
  }
}
