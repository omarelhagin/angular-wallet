import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../models/user';
import { AppConfigService } from './app-config.service';
import { Status } from '../models/status';

@Injectable({
  providedIn: 'root'
})
export class CloudantService {
  httpHeaders = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': environment.authToken
    })
  };

  constructor(private http: HttpClient,
    private appConfigService: AppConfigService) { }

  signIn(email: string, password: string): Observable<Status> {
    return this.getUserByEmail(email)
    .pipe(
      map(response => {
        if (response.docs.length !== 0) {
          if (password === response.docs[0].password) {
            this.appConfigService.loggedInUser = response.docs[0];
            sessionStorage.setItem('user', JSON.stringify(response.docs[0]));
            return Status.OK;
          } else {
            return Status.ERR_PASS;
          }
      } else {
        return Status.ERR_EMAIL;
      }
    })
    );
  }

  signOut() {
    this.appConfigService.loggedInUser = null;
    sessionStorage.removeItem('user');
  }

  createUserAndGetId(user: User): Observable<any> {
    return this.http
    .post(`${environment.cloudantUrl}/${environment.cloudantDatabase}`,
    user, this.httpHeaders)
    .pipe(
      catchError(this.handleError<any>('createUserAndGetId'))
    );
  }

  getUserByEmail(email: string): Observable<any> {
    return this.http
    .post(`${environment.cloudantUrl}/${environment.cloudantDatabase}/_find`,
    {
      'selector': {
        'email': email
      },
      'use_index': environment.designDocument
    }, this.httpHeaders)
    .pipe(
      catchError(this.handleError<any>('getUserByEmail'))
    );
  }

  updateUserDocument(id: string, newUserDocument: User): Observable<any> {
    return this.http.put(`${environment.cloudantUrl}/${environment.cloudantDatabase}/${id}`,
    newUserDocument, this.httpHeaders)
    .pipe(
      catchError(this.handleError<any>('updateUserDocument'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      alert(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
