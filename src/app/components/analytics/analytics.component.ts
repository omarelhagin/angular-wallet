import { Component, OnInit } from '@angular/core';
import { Expense } from 'src/app/models/expense';
import { AppConfigService } from 'src/app/services/app-config.service';
import * as CanvasJS from '../../../assets/canvasjs.min.js';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {

  constructor(private appConfigService: AppConfigService) { }

  ngOnInit() {
    const expenses = this.appConfigService.loggedInUser.expenses;
    const pieChart = new CanvasJS.Chart('pieChartContainer', {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: 'Expenses By Category'
      },
      data: [{
        type: 'pie',
        startAngle: 240,
        yValueFormatString: '##0.00"%"',
        indexLabel: '{label} {y}',
        dataPoints: this.constructPieChartDataPoints(expenses)
      }]
    });

    const columnChart = new CanvasJS.Chart('columnChartContainer', {
      animationEnabled: true,
      theme: 'light2',
      title: {
        text: 'Money Spent Per Expense Category'
      },
      axisY: {
        title: 'Amount Spent ($)'
      },
      data: [{
        type: 'column',
        showInLegend: false,
        dataPoints: this.constructColumnChartDataPoints(expenses)
      }]
    });
    pieChart.render();
    columnChart.render();
  }

  constructPieChartDataPoints(expenses: Expense[]) {
    const foodRatio = expenses.filter((obj) => obj.category === 'Food').length / expenses.length * 100;
    const transportRatio = expenses.filter((obj) => obj.category === 'Transport').length / expenses.length * 100;
    const accomRatio = expenses.filter((obj) => obj.category === 'Accommodation').length / expenses.length * 100;
    const entertainRatio = expenses.filter((obj) => obj.category === 'Entertainment').length / expenses.length * 100;
    const sportsRatio = expenses.filter((obj) => obj.category === 'Sports').length / expenses.length * 100;
    const healthcareRatio = expenses.filter((obj) => obj.category === 'Healthcare').length / expenses.length * 100;
    const clothesRatio = expenses.filter((obj) => obj.category === 'Clothes').length / expenses.length * 100;
    const billsRatio = expenses.filter((obj) => obj.category === 'Bills').length / expenses.length * 100;
    const otherRatio = expenses.filter((obj) => obj.category === 'Other').length / expenses.length * 100;
    return [
      {label: 'Food', y: foodRatio},
      {label: 'Transport', y: transportRatio},
      {label: 'Accommodation', y: accomRatio},
      {label: 'Entertainment', y: entertainRatio},
      {label: 'Sports', y: sportsRatio},
      {label: 'Healthcare', y: healthcareRatio},
      {label: 'Clothes', y: clothesRatio},
      {label: 'Bills', y: billsRatio},
      {label: 'Other', y: otherRatio}
    ];
  }

  constructColumnChartDataPoints(expenses: Expense[]) {
    const foodSpentSum = expenses.filter((obj) => obj.category === 'Food')
    .reduce((accumulator, expense) => accumulator + Number(expense.amount), 0);
    const transportSpentSum = expenses.filter((obj) => obj.category === 'Transport')
    .reduce((accumulator, expense) => accumulator + Number(expense.amount), 0);
    const accomSpentSum = expenses.filter((obj) => obj.category === 'Accommodation')
    .reduce((accumulator, expense) => accumulator + Number(expense.amount), 0);
    const entertainSpentSum = expenses.filter((obj) => obj.category === 'Entertainment')
    .reduce((accumulator, expense) => accumulator + Number(expense.amount), 0);
    const sportsSpentSum = expenses.filter((obj) => obj.category === 'Sports')
    .reduce((accumulator, expense) => accumulator + Number(expense.amount), 0);
    const healthcareSpentSum = expenses.filter((obj) => obj.category === 'Healthcare')
    .reduce((accumulator, expense) => accumulator + Number(expense.amount), 0);
    const clothesSpentSum = expenses.filter((obj) => obj.category === 'Clothes')
    .reduce((accumulator, expense) => accumulator + Number(expense.amount), 0);
    const billsSpentSum = expenses.filter((obj) => obj.category === 'Bills')
    .reduce((accumulator, expense) => accumulator + Number(expense.amount), 0);
    const otherSpentSum = expenses.filter((obj) => obj.category === 'Other')
    .reduce((accumulator, expense) => accumulator + Number(expense.amount), 0);
    return [
      {label: 'Food', y: foodSpentSum},
      {label: 'Transport', y: transportSpentSum},
      {label: 'Accommodation', y: accomSpentSum},
      {label: 'Entertainment', y: entertainSpentSum},
      {label: 'Sports', y: sportsSpentSum},
      {label: 'Healthcare', y: healthcareSpentSum},
      {label: 'Clothes', y: clothesSpentSum},
      {label: 'Bills', y: billsSpentSum},
      {label: 'Other', y: otherSpentSum}
    ];
  }
}
