import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';

import { CATEGORIES } from 'src/app/models/categories';
import { Expense } from 'src/app/models/expense';
import { AppConfigService } from 'src/app/services/app-config.service';
import { CloudantService } from 'src/app/services/cloudant.service';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.css']
})
export class ExpensesComponent implements OnInit {

  @ViewChild('expenseFormElement') expenseFormElement;
  categories: string[];
  expenseForm: FormGroup;

  constructor(public appConfigService: AppConfigService,
    private cloudantService: CloudantService) {
    this.categories = CATEGORIES;
    this.expenseForm = new FormGroup({
      nameInput: new FormControl(null, Validators.required),
      amountInput: new FormControl(null,
         [Validators.required, Validators.min(1), Validators.pattern('[0-9]*')]),
      categorySelect: new FormControl(null, Validators.required),
      dateInput: new FormControl(null, Validators.required)
    });
  }

  ngOnInit() {
  }

  addExpense() {
    const name = this.expenseForm.get('nameInput').value;
    const amount = this.expenseForm.get('amountInput').value;
    const selectedCategory = this.expenseForm.get('categorySelect').value;
    const selectedDate = this.expenseForm.get('dateInput').value;

    const loggedInUser = this.appConfigService.loggedInUser;
    loggedInUser.expenses
    .push(new Expense(name, amount, selectedCategory, selectedDate));
    this.cloudantService.updateUserDocument(loggedInUser._id, loggedInUser)
    .subscribe(response => {
      loggedInUser._rev = response.rev;
    });
    this.expenseFormElement.resetForm();
  }
}
