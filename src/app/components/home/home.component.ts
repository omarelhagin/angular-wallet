import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CloudantService } from 'src/app/services/cloudant.service';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private cloudantService: CloudantService,
    public appConfigService: AppConfigService,
    public router: Router) {}

  ngOnInit() {}

  signOut() {
    this.cloudantService.signOut();
  }
}
