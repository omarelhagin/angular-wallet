import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CloudantService } from 'src/app/services/cloudant.service';
import { User } from 'src/app/models/user';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignUpComponent implements OnInit {
  userAlreadyExists = false;
  signUpForm = new FormGroup({
    emailInput: new FormControl(null, [Validators.required, Validators.email]),
    passwordInput: new FormControl(null, Validators.required),
    firstNameInput: new FormControl(null, Validators.required),
    lastNameInput: new FormControl(null, Validators.required)
  });
  isFormSubmitted = false;

  constructor(
    private authService: CloudantService,
    private appConfigService: AppConfigService,
    private router: Router) { }

  ngOnInit() {
  }

  signUp() {
    this.userAlreadyExists = false;
    this.isFormSubmitted = true;
    const email = this.signUpForm.get('emailInput').value;
    const password = this.signUpForm.get('passwordInput').value;
    const firstName = this.signUpForm.get('firstNameInput').value;
    const lastName = this.signUpForm.get('lastNameInput').value;
    const newUser = new User(email, password, firstName, lastName);

    this.authService.getUserByEmail(email)
    .subscribe(response => {
      if (response.docs.length === 0) {
        this.authService.createUserAndGetId(newUser)
        .subscribe(createdUser => {
          newUser._id = createdUser.id;
          newUser._rev = createdUser.rev;
          this.appConfigService.loggedInUser = newUser;
          sessionStorage.setItem('user', JSON.stringify(newUser));
          this.router.navigate(['/']);
        });
      } else {
        this.userAlreadyExists = true;
        this.isFormSubmitted = false;
      }
    });
  }
}
