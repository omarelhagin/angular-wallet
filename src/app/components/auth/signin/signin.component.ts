import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Status } from 'src/app/models/status';
import { CloudantService } from 'src/app/services/cloudant.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SignInComponent implements OnInit {
  userAlreadyExists = false;
  signInForm = new FormGroup({
    emailInput: new FormControl(null, [Validators.required, Validators.email]),
    passwordInput: new FormControl(null, Validators.required)
  });
  isFormSubmitted = false;
  incorrectEmail = false;
  incorrectPassword = false;

  constructor(
    private cloudantService: CloudantService,
    private router: Router) { }

  ngOnInit() {
  }

  signIn() {
    this.incorrectEmail = false;
    this.incorrectPassword = false;
    this.isFormSubmitted = true;
    const email = this.signInForm.get('emailInput').value;
    const password = this.signInForm.get('passwordInput').value;

    this.cloudantService.signIn(email, password)
    .subscribe(resultStatus => {
      if (resultStatus === Status.OK) {
        this.router.navigate(['/']);
      } else {
        this.isFormSubmitted = false;
        if (resultStatus === Status.ERR_PASS) {
          this.incorrectPassword = true;
        } else if (resultStatus === Status.ERR_EMAIL) {
          this.incorrectEmail = true;
        }
      }
    });
  }
}
