export const environment = {
  production: true,
  cloudantUrl: 'https://5b507902-ef8d-456f-894e-18e98a5b616e-bluemix:'
  + '7999e08f478d28f5af7df823498b2ba0df7835f9ebde3e2bbbc2857588ead35f'
  + '@5b507902-ef8d-456f-894e-18e98a5b616e-bluemix.cloudantnosqldb.appdomain.cloud',
  cloudantDatabase: 'angular-wallet-db',
  authToken: 'Basic NWI1MDc5MDItZWY4ZC00NTZmLTg5NGUtMThlOThhNWI2MTZlLWJsd' +
  'WVtaXg6Nzk5OWUwOGY0NzhkMjhmNWFmN2RmODIzNDk4YjJiYTBkZjc4MzVmOWViZGUzZTJiYmJjM' +
  'jg1NzU4OGVhZDM1Zg==',
  designDocument: '_design/442b6aed25b83a0a8bbb398ef3f2f37b97c112de'
};
